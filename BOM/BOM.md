| Name  | Price (EUR)  | Price (UAH) |
|:------------- |:---------------:| -------------:|
| Arduino Mega         | 14,29          | 600        |
| Arduino mega ramps 1.4         | 4,76          | 200        |
| Display LCD 12864 with an adapter for RAMPS 1.4         | 13,10          | 550        |
| NEMA17 stepper motor         | 7,86          | 330        |
| Stepper motor driver A4988         | 1,43          | 60        |
| Extruder heater         | 1,43          | 60        |
| Thermistor         | 0,95          | 40        |
| Aluminum heating block         | 1,19          | 50        |
| Nozzle for a 3D printer         | 1,19          | 50        |
|  Power Supply 12V 10A        | 8,45          | 355        |
|          | Total price          | Total price        |
|          | 54,64          | 2295        |
