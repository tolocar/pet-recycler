# PET recycler

![Alt text](<Design files/Pet_Recycler_V2_2024-May-10_11-15-10AM-000_CustomizedView3578226195.png>)

# Description

this is repository for PET recycler 

## Technology

# Key resources

# [BOM](BOM/Recycler_BOM_-_%D0%9B%D0%B8%D1%81%D1%821.csv)


| Name  | Price (EUR)  | Price (UAH) |
|:------------- |:---------------:| -------------:|
| Arduino Mega         | 14,29          | 600        |
| Arduino mega ramps 1.4         | 4,76          | 200        |
| Display LCD 12864 with an adapter for RAMPS 1.4         | 13,10          | 550        |
| NEMA17 stepper motor         | 7,86          | 330        |
| Stepper motor driver A4988         | 1,43          | 60        |
| Extruder heater         | 1,43          | 60        |
| Thermistor         | 0,95          | 40        |
| Aluminum heating block         | 1,19          | 50        |
| Nozzle for a 3D printer         | 1,19          | 50        |
|  Power Supply 12V 10A        | 8,45          | 355        |
|          | Total price          | Total price        |
|          | 54,64          | 2295        |

# License

## Hardware design, CAD and PCB files, BOM, settings and other technical or design files are released under the following license:

* CERN Open Hardware Licence Version 2 Weakly Reciprocal - CERN-OHL-W


* Assembly manual, pictures, videos, presentations, description text and other type of media are released under the following license:

* Creative-Commons-Attribution-ShareAlike 4.0 International - CC BY-SA 4.0